const Taller = require('../models').Taller;
const Tallerista = require('../models').Tallerista;

module.exports = {

    async getAllTaller(req,res) {
        try {

            const tallerCollection = await Taller.findAll({
                attributes: ['id', 'puntaje','co2generado','id_tallerista']
            });

            res.status(201).send(tallerCollection);

        }
        catch(e){
            console.log(e);

            res.status(500).send(e);
        }

    },
    async search_taller(req, res) {
        try {
            const talleristaCollection = await Tallerista.findOne({where: { id_tallerista: req.body.id_tallerista}})
            res.status(201).send(talleristaCollection);
        }
        catch(e){
            console.log(e);
            res.status(400).send(e);
        }
                    
    },
    async create(req,res) {
        try {
            console.log(req.body);
            const tallerCollection = await Taller.create({
                id_tallerista : req.body.id_tallerista,
                puntaje : req.body.puntaje,
                co2generado : req.body.co2generado,
				id: req.body.id
            });
            res.status(201).send(tallerCollection);
        }
        catch(e){
            console.log(e);
            res.status(400).send(e);
        }
                    
    },

    async update(req,res) {

        try{
            const tallerCollection = await Taller.findByPk(req.params.id);
            if(tallerCollection){
                const updatedtaller = await Taller.update({
                    puntaje : req.body.puntaje,
                    co2generado : req.body.co2generado,
                    id_tallerista : req.body.id_tallerista,
                }, {where: { id: tallerCollection.id}});

                res.status(201).send(updatedtaller)

            }
            else{

                res.status(404).send("Taller no encontrado");
            }

        }
        catch(e){
            console.log(e);

            res.status(500).send(e);

        }
    },

    async delete(req,res) {

        try{
            const tallerCollection = await Taller.findByPk(req.params.id);
            if(tallerCollection){
                const updatedtaller = await Taller.destroy( 
                    {where: { id: tallerCollection.id}});

                res.status(201).send(updatedtaller.toString())

            }
            else{

                res.status(404).send("Taller ro encontrado");
            }

        }
        catch(e){
            console.log(e);

            res.status(500).send(e);

        }
    }
}
