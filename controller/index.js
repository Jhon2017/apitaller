const Tallerista = require('./tallerista');
const Pregunta = require('./pregunta');
const Respuesta = require('./respuesta');
const Taller = require('./taller');
const Bot = require('./bot');
module.exports = {
    Tallerista,
    Pregunta,
    Respuesta,
    Taller,
    Bot
}