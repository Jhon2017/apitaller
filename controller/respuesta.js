const Respuesta = require('../models').Respuesta;

module.exports = {

    async getAllRespuestas(req,res) {
        try {

            const respuestaCollection = await Respuesta.findAll({
                attributes: ['id', 'id_pregunta','respuesta','correct']
            });

            res.status(201).send(respuestaCollection);

        }
        catch(e){
            console.log(e);

            res.status(500).send(e);
        }

    },

    async create(req,res) {
        try {
            const respuestaCollection = await Respuesta.create({
                id_pregunta : req.body.id_pregunta,
                respuesta : req.body.respuesta,
                correct : req.body.correct,
            });
            res.status(201).send(respuestaCollection);
        }
        catch(e){
            console.log(e);
            res.status(400).send(e);
        }
                    
    },

    async update(req,res) {

        try{
            const respuestaCollection = await Respuesta.findByPk(req.params.id);
            if(respuestaCollection){
                const updatedrespuesta = await Respuesta.update({
                    id_pregunta : req.body.id_pregunta,
                    respuesta : req.body.respuesta,
                    correct : req.body.correct,
                }, {where: { id: respuestaCollection.id}});

                res.status(201).send(updatedrespuesta)

            }
            else{

                res.status(404).send("Respuesta ro encontrado");
            }

        }
        catch(e){
            console.log(e);

            res.status(500).send(e);

        }
    },

    async delete(req,res) {

        try{
            const respuestaCollection = await Respuesta.findByPk(req.params.id);
            if(respuestaCollection){
                const updatedrespuesta = await Respuesta.destroy( 
                    {where: { id: respuestaCollection.id}});

                res.status(201).send(updatedrespuesta.toString())

            }
            else{

                res.status(404).send("Respuesta ro encontrado");
            }

        }
        catch(e){
            console.log(e);

            res.status(500).send(e);

        }
    }

}