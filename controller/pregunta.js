const Preguntas = require('../models').Pregunta;
const Respuesta = require('../models').Respuesta;

module.exports = {

    async getAllPreguntas(req,res) {
        try {

            const PreguntasCollection = await Preguntas.findAll({
                attributes: ['id', 'pregunta','valor']
            });

            res.status(201).send(PreguntasCollection);

        }
        catch(e){
            console.log(e);

            res.status(500).send(e);
        }

    },

    async create(req,res) {
        try {
            const PreguntasCollection = await Preguntas.create({
                pregunta : req.body.pregunta,
                valor : req.body.valor,
            });
            res.status(201).send(PreguntasCollection);
        }
        catch(e){
            console.log(e);
            res.status(400).send(e);
        }
                    
    },

    async update(req,res) {

        try{
            const PreguntasCollection = await Preguntas.findByPk(req.params.id);
            if(PreguntasCollection){
                const updatedPreguntas = await Preguntas.update({
                    pregunta : req.body.pregunta,
                    valor : req.body.valor,
                }, {where: { id: PreguntasCollection.id}});

                res.status(201).send(updatedPreguntas)

            }
            else{

                res.status(404).send("Preguntas ro encontrado");
            }

        }
        catch(e){
            console.log(e);

            res.status(500).send(e);

        }
    },

    async delete(req,res) {

        try{
            const PreguntasCollection = await Preguntas.findByPk(req.params.id);
            if(PreguntasCollection){
                const updatedPreguntas = await Preguntas.destroy( 
                    {where: { id: PreguntasCollection.id}});

                res.status(201).send(updatedPreguntas.toString())

            }
            else{

                res.status(404).send("Preguntas ro encontrado");
            }

        }
        catch(e){
            console.log(e);

            res.status(500).send(e);

        }
    },

    async preguntaRespuesta(req, res){
        try{
            Preguntas.findAll({
                include: [{model: Respuesta, as: 'respuesta'}]
            }).then(preguntas =>{
        const resObj = preguntas.map(pregunta => {
        return Object.assign(
          {},
          {
            pregunta_id: pregunta.id,
            pregunta: pregunta.pregunta,
            valor: pregunta.valor,
            respuestas: pregunta.respuesta.map(respuesta => {
              return Object.assign(
                {},
                {
                  respuesta_id: respuesta.id,
                  respuesta: respuesta.respuesta,
                  correct: respuesta.correct,
                })
            })
          })
      });
      res.json(resObj)
    });
        }
        catch(e){
            console.log(e);
            res.status(500).send(e);

        }
    }

}