const Tallerista = require('../models').Tallerista;
const Taller = require('../models').Taller;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
module.exports = {

    async getAllTalleristas(req,res) {
        try {

            const talleristaCollection = await Tallerista.findAll({
                attributes: ['id', 'name','last_name', 'nickname','email', 'birthday']
            });
            res.status(201).send(talleristaCollection);

        }
        catch(e){
            console.log(e);

            res.status(500).send(e);
        }

    },
    async search_nick_email(req, res) {
        try {
            const talleristaCollection = await Tallerista.findOne({ where: {[Op.or]: [{nickname: req.body.nickname}, {email: req.body.email}]} })
            res.status(201).send(talleristaCollection);
        }
        catch(e){
            console.log(e);
            res.status(400).send(e);
        }
                    
    },

    async create(req,res) {
        try {
            const talleristaCollection = await Tallerista.create({
                name : req.body.name,
                last_name : req.body.last_name,
                email : req.body.email,
                nickname : req.body.nickname,
                birthday : req.body.birthday,
                password : req.body.password,
                rol: req.body.rol
            });
            res.status(201).send(talleristaCollection);
        }
        catch(e){
            console.log(e);
            res.status(400).send(e);
        }
                    
    },

    async update(req,res) {

        try{
            const talleristaCollection = await Tallerista.findByPk(req.params.id);
            if(talleristaCollection){
                const updatedtallerista = await Tallerista.update({
                    name : req.body.name,
                    last_name : req.body.last_name,
                    email : req.body.email,
                    nickname : req.body.nickname,
                    birthday : req.body.birthday,
                    password : req.body.password,
                    rol: req.body.rol
                }, {where: { id: talleristaCollection.id}});

                res.status(201).send(updatedtallerista)

            }
            else{

                res.status(404).send("Tallerista ro encontrado");
            }

        }
        catch(e){
            console.log(e);

            res.status(500).send(e);

        }
    },

    async delete(req,res) {

        try{
            const talleristaCollection = await Tallerista.findByPk(req.params.id);
            if(talleristaCollection){
                const updatedtallerista = await Tallerista.destroy( 
                    {where: { id: talleristaCollection.id}});
                res.status(201).send(updatedtallerista.toString())

            }
            else{

                res.status(404).send("Tallerista ro encontrado");
            }

        }
        catch(e){
            console.log(e);

            res.status(500).send(e);

        }
    },
    async tallerTallerista(req, res){
        try{
            Tallerista.findAll({
                include: [{model: Taller, as: 'taller', required: true}],
                 order: [[{model: Taller, as: 'taller'}, 'puntaje', 'DESC']]
            }).then(talleristas =>{
        const resObj = talleristas.map(tallerista => {
        return Object.assign(
          {},
          {
            tallerista_id: tallerista.id,
            nickname: tallerista.nickname,
            puntaje: tallerista.taller.puntaje,
          })
      });
      res.json(resObj)
    });
        }
        catch(e){
            console.log(e);
            res.status(500).send(e);

        }
    }


}