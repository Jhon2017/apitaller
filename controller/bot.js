const dgram = require('dgram');
const PORT = 10000;
const HOST = "localhost";
const TIMEOUT = 1000;

module.exports = {
	async send(req, res) {
        try {
        	const client = dgram.createSocket('udp4');
        	const message = Buffer.from("{"+"\"message\""+":"+"\""+req.body.message+"\""+", "+"\"uuid\""+":"+"\""+req.body.uuid+"\""+"}");
        	client.send(message, PORT, HOST, (err) => {
        	   if(err){
        	     console.log(error);
        	   }
        	});

            client.on('message', (msg, info) => {
                res.set('Content-Type', 'text/plain');
            	res.status(201).send(msg);
            	client.close();
            });
        }
        catch(e){
            console.log(e);
            res.status(400).send(e);
        }
                    
    },


}