const talleristaController = require('../controller').Tallerista;
const preguntaController = require('../controller').Pregunta;
const respuestaController = require('../controller').Respuesta;
const tallerController = require('../controller').Taller;
const botController = require('../controller').Bot;

module.exports = (app) => {
    app.get('/api',(req,res) => {
        res.status(200).send({
            data : "Bievenido api taller"
        })
    })
    //bot
    app.post('/api/bot', botController.send);
    //tallerista
    app.post('/api/tallerista/nickname_email',talleristaController.search_nick_email);
    app.get('/api/tallerista',talleristaController.getAllTalleristas);
    app.post('/api/tallerista',talleristaController.create);
    app.put('/api/tallerista/:id',talleristaController.update);
    app.delete('/api/tallerista/:id',talleristaController.delete);
    app.get('/api/tallertallerista',talleristaController.tallerTallerista);
    //pregunta
    app.get('/api/pregunta',preguntaController.getAllPreguntas);
    app.get('/api/preguntarespuesta',preguntaController.preguntaRespuesta);
    app.post('/api/pregunta',preguntaController.create);
    app.put('/api/pregunta/:id',preguntaController.update);
    app.delete('/api/pregunta/:id',preguntaController.delete);
    //respuesta
    app.get('/api/respuesta',respuestaController.getAllRespuestas);
    app.post('/api/respuesta',respuestaController.create);
    app.put('/api/respuesta/:id',respuestaController.update);
    app.delete('/api/respuesta/:id',respuestaController.delete);
     //Taller
    app.get('/api/taller',tallerController.getAllTaller);
    app.post('/api/taller',tallerController.create);
    app.post('/api/taller/search_idtallerista',tallerController.search_taller);
    app.put('/api/taller/:id',tallerController.update);
    app.delete('/api/taller/:id',tallerController.delete);



}