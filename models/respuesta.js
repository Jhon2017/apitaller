'use strict';
module.exports = (sequelize, DataTypes) => {
  const Respuesta = sequelize.define('Respuesta', {
    id_pregunta: DataTypes.INTEGER,
    respuesta: DataTypes.STRING,
    correct: DataTypes.BOOLEAN
  }, {timestamps: false});
  Respuesta.associate = function(models) {
    // associations can be defined here
  };
  return Respuesta;
};