'use strict';
module.exports = (sequelize, DataTypes) => {
  const Dispositivo = sequelize.define('Dispositivo', {
    descripcion: DataTypes.STRING,
    potencia: DataTypes.FLOAT,
    tipo: DataTypes.STRING
  }, {});
  Dispositivo.associate = function(models) {
    // associations can be defined here
    Dispositivo.hasMany(models.UsuarioDispositivo, {as: 'dispositivoId',foreignKey: 'id_dispositivo'});
  };
  return Dispositivo;
};