'use strict';
module.exports = (sequelize, DataTypes) => {
  const Tallerista = sequelize.define('Tallerista', {
    name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    nickname: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    birthday: DataTypes.DATE,
    rol: DataTypes.BOOLEAN
  }, {});
  Tallerista.associate = function(models) {
     Tallerista.hasOne(models.Taller, {as: 'taller',foreignKey: 'id_tallerista'});
  };
  return Tallerista;
};
