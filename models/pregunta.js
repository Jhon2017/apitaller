'use strict';
module.exports = (sequelize, DataTypes) => {
  const Pregunta = sequelize.define('Pregunta', {
    pregunta: DataTypes.STRING,
    valor: DataTypes.INTEGER
  }, {timestamps: false});
  Pregunta.associate = function(models) {
    // associations can be defined here
    Pregunta.hasMany(models.Respuesta, {as: 'respuesta',foreignKey: 'id_pregunta'});
  };
  return Pregunta;
};