'use strict';
module.exports = (sequelize, DataTypes) => {
  const UsuarioDispositivo = sequelize.define('UsuarioDispositivo', {
    id_tallerista: DataTypes.INTEGER,
    id_dispositivo: DataTypes.INTEGER,
    cantidad: DataTypes.INTEGER,
    hora_uso_dia: DataTypes.INTEGER
  }, {});
  UsuarioDispositivo.associate = function(models) {

  };
  return UsuarioDispositivo;
};