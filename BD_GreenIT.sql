-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.8-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando datos para la tabla taller.dispositivo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `dispositivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `dispositivo` ENABLE KEYS */;

-- Volcando datos para la tabla taller.pregunta: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `pregunta` DISABLE KEYS */;
INSERT INTO `pregunta` (`id`, `pregunta`, `valor`) VALUES
	(1, '¿Qué es greenIT?', 1),
	(2, '¿Qué es el CO2?', 1),
	(3, '¿Cuál dispositivo genera más CO2?', 1),
	(4, 'La quema de combustibles fósiles puede causar...', 1),
	(5, 'El cambio climático se produce debido..', 1);
/*!40000 ALTER TABLE `pregunta` ENABLE KEYS */;

-- Volcando datos para la tabla taller.respuesta: ~20 rows (aproximadamente)
/*!40000 ALTER TABLE `respuesta` DISABLE KEYS */;
INSERT INTO `respuesta` (`id`, `id_pregunta`, `respuesta`, `correct`) VALUES
	(1, 1, 'Una cultura europea', 0),
	(2, 1, 'Buenas prácticas tecnológicas', 1),
	(3, 1, 'Una solucion para el derretimiento de los polos', 0),
	(4, 1, 'todas las anteriores', 0),
	(5, 2, 'Oxigeno que se escapa de la atmósfera', 0),
	(6, 2, 'Nivel de mercurio en el mar', 0),
	(7, 2, 'Dióxido de carbono', 1),
	(8, 2, 'Carbono oxigenado', 0),
	(9, 3, 'PC de escritorio', 1),
	(10, 3, 'Smartphone', 0),
	(11, 3, 'Smart TV', 0),
	(12, 3, 'Portatil', 0),
	(13, 4, 'Niebla tóxica', 0),
	(14, 4, 'Huella de carbono', 0),
	(15, 4, 'Lluvia ácida', 1),
	(16, 4, 'Terremotos', 0),
	(17, 5, 'al agujero en la capa de ozono', 1),
	(18, 5, 'al deterioro de la atmosfera', 0),
	(19, 5, 'a la ausencia de lluvia', 0),
	(20, 5, 'todas las anteriores', 0);
/*!40000 ALTER TABLE `respuesta` ENABLE KEYS */;

-- Volcando datos para la tabla taller.sequelizemeta: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `sequelizemeta` DISABLE KEYS */;
INSERT INTO `sequelizemeta` (`name`) VALUES
	('20191013071827-create-tallerista.js'),
	('20191013071854-create-pregunta.js'),
	('20191013071918-create-respuesta.js'),
	('20191019153115-create-taller.js'),
	('20191030215617-create-dispositivo.js'),
	('20191031124046-create-usuario-dispositivo.js');
/*!40000 ALTER TABLE `sequelizemeta` ENABLE KEYS */;

-- Volcando datos para la tabla taller.taller: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `taller` DISABLE KEYS */;
INSERT INTO `taller` (`id`, `puntaje`, `co2generado`, `id_tallerista`) VALUES
	(2, 10, 30, 8),
	(4, 2, 0, 12),
	(5, 5, 0, 5),
	(8, 4, 0, 6);
/*!40000 ALTER TABLE `taller` ENABLE KEYS */;

-- Volcando datos para la tabla taller.tallerista: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `tallerista` DISABLE KEYS */;
INSERT INTO `tallerista` (`id`, `name`, `last_name`, `nickname`, `password`, `email`, `birthday`, `rol`) VALUES
	(1, 'Jhon', 'Rodriguez', 'jfreddy', 'hola1234', 'jhon@', '2017-08-01 00:00:00', 1),
	(2, 'Steven', 'Ossa', 'sossa', 'hola5654', 'steven@', '1998-08-01 00:00:00', 0),
	(5, '', '', 'Steven Ossa', '', 'stevenossa321@gmail.com', '1998-08-01 00:00:00', NULL),
	(6, '', '', 'Johan Ossa Serna', '', 'johanossa697@gmail.com', '1998-08-01 00:00:00', NULL),
	(8, NULL, NULL, 'sekiro', NULL, 'hello@outlast.es', NULL, NULL),
	(12, '', '', 'STEVEN OSSA SERNA', '', 'steven.ossa01@uceva.edu.co', '2020-03-01 00:00:00', NULL);
/*!40000 ALTER TABLE `tallerista` ENABLE KEYS */;

-- Volcando datos para la tabla taller.usuariodispositivo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuariodispositivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuariodispositivo` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
