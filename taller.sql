-- MySQL dump 10.17  Distrib 10.3.17-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: taller
-- ------------------------------------------------------
-- Server version	10.3.17-MariaDB-0+deb10u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Dispositivo`
--

DROP TABLE IF EXISTS `Dispositivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Dispositivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  `potencia` float DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Dispositivo`
--

LOCK TABLES `Dispositivo` WRITE;
/*!40000 ALTER TABLE `Dispositivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `Dispositivo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Pregunta`
--

DROP TABLE IF EXISTS `Pregunta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pregunta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pregunta` varchar(255) DEFAULT NULL,
  `valor` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Pregunta`
--

LOCK TABLES `Pregunta` WRITE;
/*!40000 ALTER TABLE `Pregunta` DISABLE KEYS */;
INSERT INTO `Pregunta` VALUES (1,'¿Qué es greenIT?',1),(2,'¿Qué es el CO2?',1),(3,'¿Cuál dispositivo genera más CO2?',1),(4,'La quema de combustibles fósiles puede causar...',1),(5,'El cambio climático se produce debido..',1);
/*!40000 ALTER TABLE `Pregunta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Respuesta`
--

DROP TABLE IF EXISTS `Respuesta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Respuesta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pregunta` int(11) DEFAULT NULL,
  `respuesta` varchar(255) DEFAULT NULL,
  `correct` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_pregunta` (`id_pregunta`),
  CONSTRAINT `Respuesta_ibfk_1` FOREIGN KEY (`id_pregunta`) REFERENCES `Pregunta` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Respuesta`
--

LOCK TABLES `Respuesta` WRITE;
/*!40000 ALTER TABLE `Respuesta` DISABLE KEYS */;
INSERT INTO `Respuesta` VALUES (1,1,'Una cultura europea',0),(2,1,'Buenas prácticas tecnológicas',1),(3,1,'Una solucion para el derretimiento de los polos',0),(4,1,'todas las anteriores',0),(5,2,'Oxigeno que se escapa de la atmósfera',0),(6,2,'Nivel de mercurio en el mar',0),(7,2,'Dióxido de carbono',1),(8,2,'Carbono oxigenado',0),(9,3,'PC de escritorio',1),(10,3,'Smartphone',0),(11,3,'Smart TV',0),(12,3,'Portatil',0),(13,4,'Niebla tóxica',0),(14,4,'Huella de carbono',0),(15,4,'Lluvia ácida',1),(16,4,'Terremotos',0),(17,5,'al agujero en la capa de ozono',1),(18,5,'al deterioro de la atmosfera',0),(19,5,'a la ausencia de lluvia',0),(20,5,'todas las anteriores',0);
/*!40000 ALTER TABLE `Respuesta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SequelizeMeta`
--

DROP TABLE IF EXISTS `SequelizeMeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SequelizeMeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SequelizeMeta`
--

LOCK TABLES `SequelizeMeta` WRITE;
/*!40000 ALTER TABLE `SequelizeMeta` DISABLE KEYS */;
INSERT INTO `SequelizeMeta` VALUES ('20191013071827-create-tallerista.js'),('20191013071854-create-pregunta.js'),('20191013071918-create-respuesta.js'),('20191019153115-create-taller.js'),('20191030215617-create-dispositivo.js'),('20191031124046-create-usuario-dispositivo.js');
/*!40000 ALTER TABLE `SequelizeMeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Taller`
--

DROP TABLE IF EXISTS `Taller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Taller` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `puntaje` int(11) DEFAULT NULL,
  `co2generado` float DEFAULT NULL,
  `id_tallerista` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tallerista` (`id_tallerista`),
  CONSTRAINT `Taller_ibfk_1` FOREIGN KEY (`id_tallerista`) REFERENCES `Tallerista` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Taller`
--

LOCK TABLES `Taller` WRITE;
/*!40000 ALTER TABLE `Taller` DISABLE KEYS */;
/*!40000 ALTER TABLE `Taller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tallerista`
--

DROP TABLE IF EXISTS `Tallerista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tallerista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `rol` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tallerista`
--

LOCK TABLES `Tallerista` WRITE;
/*!40000 ALTER TABLE `Tallerista` DISABLE KEYS */;
INSERT INTO `Tallerista` VALUES (1,'Jhon','Rodriguez','jfreddy','hola1234','jhon@','2017-08-01 00:00:00',1),(2,'Steven','Ossa','sossa','hola5654','steven@','1998-08-01 00:00:00',0);
/*!40000 ALTER TABLE `Tallerista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UsuarioDispositivo`
--

DROP TABLE IF EXISTS `UsuarioDispositivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UsuarioDispositivo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tallerista` int(11) DEFAULT NULL,
  `id_dispositivo` int(11) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `hora_uso_dia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tallerista` (`id_tallerista`),
  KEY `id_dispositivo` (`id_dispositivo`),
  CONSTRAINT `UsuarioDispositivo_ibfk_1` FOREIGN KEY (`id_tallerista`) REFERENCES `Tallerista` (`id`) ON DELETE CASCADE,
  CONSTRAINT `UsuarioDispositivo_ibfk_2` FOREIGN KEY (`id_dispositivo`) REFERENCES `Dispositivo` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UsuarioDispositivo`
--

LOCK TABLES `UsuarioDispositivo` WRITE;
/*!40000 ALTER TABLE `UsuarioDispositivo` DISABLE KEYS */;
/*!40000 ALTER TABLE `UsuarioDispositivo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-09 16:58:35
