'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('UsuarioDispositivo', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_tallerista: {
        type: Sequelize.INTEGER,
        onDelete : 'CASCADE',
        references : {
          model : 'Tallerista',
          key : 'id',
        },
      },
      id_dispositivo: {
        type: Sequelize.INTEGER,
        onDelete : 'CASCADE',
        references : {
          model : 'Dispositivo',
          key : 'id',
        },
      },
      cantidad: {
        type: Sequelize.INTEGER
      },
      hora_uso_dia: {
        type: Sequelize.INTEGER
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('UsuarioDispositivo');
  }
};
