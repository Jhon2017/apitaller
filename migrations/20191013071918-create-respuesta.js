'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Respuesta', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      id_pregunta: {
        type: Sequelize.INTEGER,
          onDelete : 'CASCADE',
            references : {
                model : 'Pregunta',
                key : 'id',
            },
      },
      respuesta: {
        type: Sequelize.STRING
      },
      correct: {
        type: Sequelize.BOOLEAN
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Respuesta');
  }
};